# TODO: add an option to define the orientation (left-to-right, top-to-bottom, right-to-left or bottom-to-top) and
#  add more command line options (output file name, orientation, transparency ratio, compression boolean,
#  gradient boolean [and value!]) and use "-xyz ..." notation

import sys
import os.path
from PIL import Image


class StackImage:
    def __init__(self, image, xy):
        self.image = image
        self.xy = xy


def generate_cross_faded_image(output_file_name, transparency_ratio, compress, gradient, *file_names):
    if not all([os.path.splitext(file_name)[1].lower() == '.jpg' for file_name in file_names]):
        print('Die Bilder müssen im .jpg-Format sein.')
        return

    # open image files and convert to format with alpha channel with PIL
    images = [Image.open(file_name).convert('RGBA') for file_name in file_names]

    # find smallest width of all images
    smallest_width = min(image.width for image in images)

    # resize all images to the smallest width (and height accordingly)
    images = [image.resize((smallest_width, int(
        image.height / (image.width / float(smallest_width))))) if image.width > smallest_width else image for image in
              images]

    # calculate final image height (transparency parts of all heights except of the last image [100% of last image])
    final_image_height = sum([int(image.height * transparency_ratio) for image in images[:-1]]) + images[-1].height

    # create the final image with the smallest width, the calculated height and all pixels transparent
    final_image = Image.new('RGBA', (smallest_width, final_image_height), (0, 0, 0, 0))

    # use a stack to store images and coordinates to paste them in the reverse order into the final image
    image_stack = []

    # loop through all images except of the last one
    current_start_height = 0
    for image in images[:-1]:
        # create a one pixel wide transparency mask with 100% transparency
        transparency_mask_image = Image.new('L', (1, image.height), 255)

        # add a transparency gradient to the last part of the image (according to the transparency ratio)
        height_visible = int(image.height * transparency_ratio)
        height_transparent = image.height - height_visible
        for pixel in range(height_transparent):
            y = height_visible + pixel
            # map the opacity range from 255 to 0 to the transparent pixels
            transparency_mask_image.putpixel((0, y), 255 - int(255 * float(pixel) / height_transparent))

        # resize and apply the transparency mask on the image
        image.putalpha(transparency_mask_image.resize(image.size))

        # add the image to the image stack
        image_stack.append(StackImage(image, (0, current_start_height)))

        # increase the start height by the amount of fully visible (not transparent) pixels
        current_start_height += height_visible

    # add the last image to the image stack
    image_stack.append(StackImage(images[-1], (0, current_start_height)))

    # loop through the images from bottom to top
    while image_stack:
        # take stack_image object from stack
        stack_image = image_stack.pop()

        # paste the image onto the final image
        final_image.alpha_composite(stack_image.image, stack_image.xy)

    # paint a black gradient on the image (top to bottom)
    if gradient:
        # create the gradient (transparency) mask
        gradient_mask_image = Image.new('L', (1, final_image.height))
        for y in range(final_image.height):
            # the top of the image is half-transparent, the bottom is completely visible
            gradient_mask_image.putpixel((0, y), 86 - int(86 * float(y) / final_image.height))

        # create a black image and apply the resized gradient mask
        black_image = Image.new('RGBA', final_image.size)
        black_image.putalpha(gradient_mask_image.resize(final_image.size))

        # rewrite the final image variable with the new image
        final_image = Image.alpha_composite(final_image, black_image)

    # convert the image back to .jpg (because there won't be transparent pixels)
    final_image = final_image.convert('RGB')

    # export the image with different widths and compress it
    for export_width in [768, 1080, 1440, 2160]:
        # resize the image proportionally
        output_image = final_image.resize(
            (export_width, int(final_image.height * (float(export_width) / final_image.width))))
        output_image.save(output_file_name + str(export_width) + '.jpg', progression=True, comment="Easteregg",
                          quality=60 if compress else 100)


if __name__ == '__main__':
    args = sys.argv
    if len(args) > 2:
        output_file_name = 'bg'
        transparency_ratio = 0.75
        compress = True
        gradient = True
        if transparency_ratio < 0 or transparency_ratio > 1:
            print('Das Transparenz-Verhältnis muss zwischen 0 und 1 (100%) liegen.')
        else:
            generate_cross_faded_image(output_file_name, transparency_ratio, compress, gradient, *args[1:])
    else:
        print('Du musst mindestens zwei Bilder angeben.')
