# Python installieren

Du musst Python installiert haben. Wichtig ist, dass die Version neuer als 3 ist (optimalerweise 3.8).

Um zu prüfen, ob es schon installiert ist, gib das in deiner Konsole ein:
```
python --version
```

# Abhängigkeiten installieren

Führe dafür diesen Befehl aus, während du dich in diesem Ordner befindest:
```
pip3 install -r requirements.txt
```

# Ausführen

Führe das Skript mit Angabe der zu verwendenden Bilder aus, um das Tool auszuführen:
```
python3 main.py <Bild 1> <Bild 2> ... <Bild n>
```

Beispiel:
```
python3 main.py P1460300.jpg P1430681.jpg P1180543.jpg
```